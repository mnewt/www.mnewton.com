---
type: wide
---

<div class="splash-container cube-pattern twelve columns">
  <div class="splash eight columns offset-by-two">
    <h1 class="bottom-padded">Projects</h1>
  </div>
</div>
&nbsp;

<div class="content container">
  <section class="project-container right row">
    <div class="project-image four columns">
      <img src="/images/VMware-SRM-Architecture.jpg">
    </div>
    <div class="project-content eight columns">
      <h2>Datacenter Migration</h2>
      <!-- <h3>City &amp; County of San Francisco</h3> -->
      <ul style="list-style-type: none;">
        <li>
          Multi-tenant infrastructure and application service provider
        </li>
        <li>
          Migrate 20 TB of Virtual Machines in four hours
        </li>
      </ul>
    </div>
  </section>
  <section class="project-container row">
    <div class="project-content eight columns">
      <h2>VDI (Virtual Desktop Infrastructure)</h2>
      <!-- <h3>San Francisco Sheriff's Department</h3> -->
      <ul style="list-style-type: none;">
        <li>
          2000 User VDI Deployment
        </li>
        <li>
          Citrix XenDesktop
        </li>
        <li>
          VMware vSphere
        </li>
        <li>
          Zero Clients
        </li>
      </ul>
    </div>
    <div class="project-image four columns">
      <img src="/images/citrix-xendesktop.png">
    </div>
  </section>
  <section class="project-container right row">
    <div class="project-image four columns">
      <img src="/images/office-server.png">
    </div>
    <div class="project-content eight columns">
      <h2>Worldwide Infrastructure, Directory, and Email</h2>
      <!-- <h3>Littler Mendelson</h3> -->
      <ul style="list-style-type: none;">
        <li>
          EMC Storage
        </li>
        <li>
          VMware vSphere
        </li>
        <li>
          Active Directory
        </li>
        <li>
          Microsoft Exchange
        </li>
      </ul>
    </div>
  </section>
  <section class="project-container row">
    <div class="project-content eight columns">
      <h2>Cloud Consolidation Return on Investment</h2>
      <!-- <h3>Various Clients</h3> -->
      <ul style="list-style-type: none;">
        <li>
          Datacenter Architecture
        </li>
        <li>
          Design and Server and Storage Virtualization Solutions
        </li>
        <li>
          Performance and Efficiency Measurement
        </li>
        <li>
          Return on Invenstment Analysis
        </li>
      </ul>
    </div>
    <div class="project-image four columns">
      <img src="/images/ascii_graph.png">
    </div>
  </section>
  <section class="project-container right row">
    <div class="project-image four columns">
      <img src="/images/active-directory-replication.jpg">
    </div>
    <div class="project-content eight columns">
      <h2>Directory and Email Consolidation</h2>
      <!-- <h3>City of Oakland</h3> -->
      <ul style="list-style-type: none;">
        <li>
          Active Directory, DNS, and DHCP Design
        </li>
        <li>
          Distributed Email Design
        </li>
        <li>
          Migration of thousands of Active Directory clients and hundreds of applications
        </li>
      </ul>
    </div>
  </section>
</div>
