---
type: wide
---

<div class="splash-container gradient-pattern twelve columns">
  <div class="splash eight columns offset-by-two">
    <h1>Matthew Sojourner Newton</h1>
    <h2>
      IT Consultant, &amp;c.<br>
      San Francisco
    </h2>
    <h4><a href="mailto:contact@mnewton.com">contact@mnewton.com</a></h4>
    <br />
  </div>
</div>
