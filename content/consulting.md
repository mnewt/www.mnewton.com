---
type: wide
---

<div class="spash-container cicada-pattern twelve columns">
  <div class="splash eight columns offset-by-two">
    <h1>IT Consulting</h1>
    <h2>
      Quality,<br>
      Integrity,<br>
      Humor.
    </h2>
    <p>
      We practice process driven IT consulting for organizations. Experience and quality begets efficiency and we deliver all three to our clients. We build elegant, robust, and rigorously tested IT solutions using a process that has been honed over 20 years.
      <br>
      <br>
    </p>
  </div>
</div>
&nbsp;

<div class="content container">

<h2>Phased Project Approach</h2>
<p>Each project is carried out in distinct, linear phases. These phases call out specific milestones that are marked by deliverables. Deliverables can take the form of documents, functional systems, or accomplishments. At the conclusion of each project phase, success is measured and evaluated by each party. Sometimes, the conclusion of a phase is the appropriate time to adjust the required project deliverables, or even its goals and scope.</p>

<h2>Phase 1: Discover</h2>
<p>
  During first phase of solution delivery, baseline expectations; environmental details; operational challenges; and project success criteria are established. Network and environment discovery is performed and assumptions made during the proposal process are validated. This process also presents an opportunity to mitigate concerns early on in the project.
</p>

<h2>Phase 2: Design &amp; Plan</h2>
<p>
  The remainder of the project is mapped and the high-level design that was presented during the proposal process is expanded. The core project documentation is drafted, typically including Project Plan, Design, Implementation Plan, and Test Plan. Collaborative design sessions allow our team to work closely with your IT operations and management staff, facilitating a reciprocal exchange as all design aspects are thoroughly discussed, determined, and iterated.
</p>

<h2>Phase 3: Test</h2>
<p>
  Build the lab environment and test it against Test Plan and Implementation Plan. Build the pilot environment and test it against Implementation Plan, User Experience, and Success Criteria. Revise Project Plan and Documentation, informed by the lessons learned.
</p>

<h2>Phase 4: Implement</h2>
<p>
  Measure twice, cut once. Most of the work is already complete before beginning the Implementation Phase. A project and subsequent operational environment will be more efficient in the long run by being deliberate and careful in planning and testing before action is taken. Further, the resulting deliverables will last longer, providing better value.
</p>

<h2>Phase 5: Wrap Up</h2>
<p>
  Support and training are provided. Documentation is finalized. Although Knowledge Transfer happens throughout the project, a special emphasis is placed on it during this phase to make sure everyone concludes the project with mutual understanding. Project debrief is held with lessons learned and outstanding issues recorded.
</p>

</div>
